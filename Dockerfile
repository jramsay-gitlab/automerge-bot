FROM golang:1.13 AS builder

RUN mkdir /mergebot
WORKDIR /mergebot
COPY go.mod .
COPY go.sum .

RUN go mod download
COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o /go/bin/automerge

FROM alpine

# need certs for HTTPS API access
RUN apk update && apk add ca-certificates

COPY --from=builder /go/bin/automerge /usr/local/bin/automerge
