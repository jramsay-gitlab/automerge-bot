package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/andreyvit/diff"
	"github.com/xanzy/go-gitlab"
)

func TestMarshalMemo(t *testing.T) {
	merge := AutoMerge{
		sourceBranch: "release/10.2",
		sourceSHA:    "403df46aa867442483a87582a160dc1357d776e2",
		targetBranch: "release/10.3",
		tempBranch:   "automerge/99f707ef/release/10.3",
	}

	merge.triggerCommit = &gitlab.Commit{
		ID:      "99f707ef02f096ed1e08d922e18da99b31d43e4f",
		ShortID: "99f707ef",
		Title:   "Fix the thing",
	}
	expectedCommitTitle := "release/10.3: Fix the thing"
	expectedCommitDescripion := `Automatic merge of **99f707ef**

<!-- json "automerge" -->
<!-- {"commit_sha":"99f707ef02f096ed1e08d922e18da99b31d43e4f"} -->
<!-- json "automerge" -->
`
	if title, description, err := merge.MarshalMemo(); err != nil {
		t.Fatal(err)
	} else if title != expectedCommitTitle {
		t.Fatalf("expected %s, got %s", expectedCommitTitle, title)
	} else if description != expectedCommitDescripion {
		t.Fatalf("unexpected description:\n%v", diff.LineDiff(expectedCommitDescripion, description))
	}

	merge.triggerCommit = nil
	merge.triggerMergeRequest = &gitlab.MergeRequest{
		IID:   71,
		Title: "Fix the other thing",
	}
	expectedMergeTitle := "release/10.3: Fix the other thing"
	expectedMergeDescripion := `Automatic merge of **!71**

<!-- json "automerge" -->
<!-- {"merge_request_iid":71} -->
<!-- json "automerge" -->
`
	if title, description, err := merge.MarshalMemo(); err != nil {
		t.Fatal(err)
	} else if title != expectedMergeTitle {
		t.Fatalf("expected %s, got %s", expectedMergeTitle, title)
	} else if description != expectedMergeDescripion {
		t.Fatalf("unexpected description:\n%v", diff.LineDiff(expectedMergeDescripion, description))
	}
}

func TestUnmarshalMemo(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(gitlabMockHandler))
	defer ts.Close()

	client, err := gitlab.NewClient("", gitlab.WithBaseURL(ts.URL))
	if err != nil {
		t.Fatal(err)
	}

	testMergeRequestDescription := `Great feature!

<!-- json "automerge" -->
<!-- {"merge_request_iid": 99} -->
<!-- json "automerge" -->
`

	if mr, commit, err := UnmarshalMemo(client, "test", testMergeRequestDescription); err != nil {
		t.Fatal(err)
	} else if commit != nil {
		t.Fatal()
	} else if mr.IID != 99 {
		t.Fatal()
	}

	testCommitDescription := `Great feature!

<!-- json "automerge" -->
<!-- {"commit_sha": "14cc2c372c7d41b8fa05f851fc9111116e121a7a"} -->
<!-- json "automerge" -->
`
	if mr, commit, err := UnmarshalMemo(client, "test", testCommitDescription); err != nil {
		t.Fatal(err)
	} else if mr != nil {
		t.Fatal()
	} else if commit.ID != "14cc2c372c7d41b8fa05f851fc9111116e121a7a" {
		t.Fatal()
	}
}
