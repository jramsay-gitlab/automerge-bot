package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/xanzy/go-gitlab"
)

func TestListStableBranches(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(gitlabMockHandler))
	defer ts.Close()

	client, err := gitlab.NewClient("", gitlab.WithBaseURL(ts.URL))
	if err != nil {
		t.Fatal(err)
	}

	branches, err := ListStableBranches(client, "example-project", "release/")
	if err != nil {
		t.Fatal(err)
	}

	if len(branches) != 4 {
		t.Fatalf("expected 4 stable branches, got %d", len(branches))
	}
}

func TestBranchIsSemver(t *testing.T) {
	if BranchIsSemver("master", "release/") != false {
		t.Fatal()
	}

	if BranchIsSemver("release/11.2-beta", "release/") != true {
		t.Fatal()
	}
}

func TestNextStableBranch(t *testing.T) {
	branches := []string{
		"release/10.4",
		"release/10.5",
		"release/10.6",
		"release/10.7",
		"release/10.8",
		"release/11.0",
		"release/11.1",
	}

	if NextStableBranch("release/10.5", branches, "master") != "release/10.6" {
		t.Fatal()
	}

	if NextStableBranch("release/10.8", branches, "master") != "release/11.0" {
		t.Fatal()
	}

	if NextStableBranch("release/11.1", branches, "master") != "master" {
		t.Fatal()
	}
}
